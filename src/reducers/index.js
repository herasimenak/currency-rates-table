import { combineReducers } from 'redux'
import currency from './currency'

const compareApp = combineReducers({
  currency
})

export default compareApp