import {
  RATES_REQUEST,
  GET_RATES,
  RATES_FAILURE,
  REFRESH_RATE,
  CURRENCY_FILTER
} from '../actions'

const initialState = {
  currency: ['USD', 'EUR'],
  rates: [],
  errors: []
}

const updateRates = (rates, currency) => {
  let newRates = []
  if (rates) {
    newRates = rates.filter(rate => currency.includes(rate.name))
  }
  return newRates
}

const updateCurrency = (currencyArr, currencyItem) => {
  if (currencyArr.includes(currencyItem)) {
    currencyArr.splice(currencyArr.indexOf(currencyItem), 1)
    return currencyArr
  }
  currencyArr.push(currencyItem)
  return currencyArr
}

const refreshRate = (newRates, stateRates, rateName) => {
  const newRate = newRates.find(obj => obj.name === rateName)
  return stateRates.map(rate => {
    if (rate.name === rateName) {
      return Object.assign({}, rate, { value: newRate.value })
    }
    return rate
  })
}

export default (state = initialState, action) => {
  switch (action.type) {
  case GET_RATES:
    const newRates = updateRates(action.payload.rates, state.currency)
    return { ...state, rates: newRates }
  case RATES_FAILURE:
    return { ...state, ...action.payload }
  case REFRESH_RATE:
    const refreshingRates = refreshRate(action.payload.rates, state.rates, action.payload.rateName)
    return { ...state, rates: refreshingRates }
  case CURRENCY_FILTER:
    const newCurrency = updateCurrency(state.currency, action.payload.currency)
    return {...state, currency: newCurrency }
  default:
    return state
  }
}