import { fetchCurrency } from '../api'
import action from './action';

export const RATES_REQUEST = 'RATES_REQUEST'
export const RATES_FAILURE = 'RATES_REQUEST'
export const GET_RATES = 'GET_RATES'
export const CURRENCY_FILTER = 'CURRENCY_FILTER'
export const REFRESH_RATE = 'REFRESH_RATE'

export const getRates = () => async dispatch => {
  dispatch(action(RATES_REQUEST))
  try {
    const rates = await fetchCurrency()
    dispatch(action(GET_RATES, { rates }))
  } catch (err) {
    dispatch({ type: RATES_FAILURE, payload: err.message })
  }
}

export const refreshRate = rateName => async dispatch => {
  dispatch(action(RATES_REQUEST))
  try {
    const rates = await fetchCurrency()
    dispatch(action(REFRESH_RATE, { rates, rateName }))
  } catch (err) {
    dispatch({ type: RATES_FAILURE, payload: err.message })
  }
}

export const showExtraRates = currency => dispatch => {
  return Promise.all([
    dispatch(action(CURRENCY_FILTER, { currency })),
    dispatch(getRates())
  ])
}
