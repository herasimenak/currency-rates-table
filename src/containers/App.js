import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as currencyActions from '../actions'
import { CurrencyBlock, Header } from '../components'

class App extends Component {
  componentDidMount() {
    this.props.actions.getRates()
    this.interval = setInterval(() => this.props.actions.getRates(), 10000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  render() {
    const { currency, rates, actions } = this.props
    if (rates.length == 0) {
      return (
        <div className='app'>
          <Header>
            {'rates table'}
          </Header>
          <div className='app__loadtext'>Loading...</div>
        </div>
      )
    }
    return (
      <div className='app'>
        <Header>
          {'rates table'}
        </Header>
        <CurrencyBlock
          currency={currency}
          rates={rates}
          refresh={actions.refreshRate}
          showExtraRates={actions.showExtraRates}
        />
      </div>
    )
  }
}

export default connect(
  state => ({
    currency: state.currency.currency,
    rates: state.currency.rates
  }),
  dispatch => ({
    actions: bindActionCreators(currencyActions, dispatch)
  })
)(App)
