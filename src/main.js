import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk'
import App from './containers/App'
import reducer from './reducers'
import './_scss/main.scss'

const createStoreWithMiddleware = applyMiddleware(
  ReduxThunk
)(createStore)

render(
  <Provider store={createStoreWithMiddleware(reducer)}>
    <App />
  </Provider>,
  document.getElementById('root')
)
