import React from 'react'

const Header = ({ children }) => (
  <div className='header'>
    <h1 className='header__text text--blue'>
      {children}
    </h1>
  </div>
)

export default Header
