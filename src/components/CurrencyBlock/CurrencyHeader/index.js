import React from 'react'
import CheckBoxItem from '../CheckBoxItem'

const CurrencyHeader = ({ showExtraRates, currency }) => (
  <div className='currency__header'>
    <div className='currency__title text--blue'>
      <span>1 RUB Rates table</span>
    </div>
    <div className='currency__chosser '>
      <div className='currency__text'>
        <span>Show:</span>
      </div>
      <CheckBoxItem
        label='CAD'
        isChecked={currency.includes('CAD')}
        showExtraRates={showExtraRates}
      />
      <CheckBoxItem
        label='JPY'
        isChecked={currency.includes('JPY')}
        showExtraRates={showExtraRates}
      />
    </div>
  </div>
)

export default CurrencyHeader
