import React from 'react'

const CheckBoxItem = ({ label, isChecked, showExtraRates }) => (
  <div className='checkbox__item'>
    <label className='checkbox'>
      <div>
        <input
          className='checkbox__control'
          checked={isChecked}
          onChange={() => showExtraRates(label)}
          type='checkbox'
        />
        <span className='checkbox__checkmark checkmark'></span>
      </div>
      {label}
    </label>
  </div>
)

export default CheckBoxItem
