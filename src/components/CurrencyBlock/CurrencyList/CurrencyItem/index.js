import React from 'react'
import Button from '../../Button'

const CurrencyItem = ({ name, value, refresh }) => (
  <div className='list__row'>
    <div className='list__column list__column--first'>
      <span>{name}</span>
    </div>
    <div className='list__column list__column--second'>
      <span>{value}</span>
      <Button onClick={() => refresh(name)}>
        {'Refresh'}
      </Button>
    </div>
  </div>
)

export default CurrencyItem
