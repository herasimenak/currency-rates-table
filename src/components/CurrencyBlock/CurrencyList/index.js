import React from 'react'
import CurrencyItem from './CurrencyItem'

const CurrencyList = ({ rates, refresh }) => (
  <div className='currency__list'>
    <div className='list__row list__row--grey'>
      <div className='list__column list__column--first'>
        <span>RUB</span>
      </div>
      <div className='list__column list__column--second'>
        <span>1.00 RUB</span>
      </div>
    </div>
    {rates.map(rate =>(
      <CurrencyItem
        key={rate.name}
        name={rate.name}
        value={rate.value}
        refresh={refresh}
      />
    ))}
  </div>
)

export default CurrencyList
