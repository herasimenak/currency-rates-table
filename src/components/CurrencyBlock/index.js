import React from 'react'
import CurrencyHeader from './CurrencyHeader'
import CurrencyList from './CurrencyList'

const CurrencyBlock = ({ currency, rates, refresh, showExtraRates }) => (
  <div className='currency'>
    <CurrencyHeader
      currency={currency}
      showExtraRates={showExtraRates}
    />
    <CurrencyList
      rates={rates}
      refresh={refresh}
    />
  </div>
)

export default CurrencyBlock
