export const convert = (rateCurrency, rateRUB) => {
  return (rateCurrency / rateRUB).toFixed(5)
}