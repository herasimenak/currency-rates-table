import { convert } from './libs/formatting'

const URL = 'https://openexchangerates.org/api/latest.json?app_id=5efee5023b804a84b4dd2c3f7830eefe'

const currency = ['USD', 'EUR', 'CAD', 'JPY']

const buildSelectedList = (data) => {
  let selectedList = []
  Object.keys(data).filter(key =>
    currency.includes(key)).map(item => {
    return selectedList.push({ name: item, value: convert(data[item], data.RUB )})
  })
  return selectedList
}

export const fetchCurrency = async () => {
  const response = await fetch(URL, { method: 'GET' })

  if (response.ok) {
    const { rates } = await response.json()
    const selectedRates = buildSelectedList(rates)
    return selectedRates
  }

  const errMessage = await response.text()
  throw new Error(errMessage)
}
